import { Global } from "@emotion/react";
import styled from "@emotion/styled";

export const GlobalStyles = styled(Global)`
  * {
    box-sizing: border-box;
    font-family: "M PLUS Rounded 1c", sans-serif;
  }

  html,
  body {
    padding: 0;
    margin: 0;
  }

  @font-face {
    font-family: matrixFont;
    src: url("../public/fonts/matrixCodeNfi.ttf");
  }

  @font-face {
    font-family: mplus;
    src: url("../public/fonts/mPlusRounded1c.ttf");
  }

  .blinking-cursor:after {
    content: "|";
    color: aqua;
    align-items: center;
    animation: blink 1s infinite;
  }

  @keyframes blink {
    50% {
      opacity: 0;
    }
  }

  @media (prefers-color-scheme: dark) {
    html {
      color-scheme: dark;
    }
    body {
      color: white;
      background: black;
    }
  }
`;
