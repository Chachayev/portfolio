import React, { useState } from "react";
import { Box, Spinner } from "@chakra-ui/react";

const Voxel: React.FC = () => {
  const [loading, setLoading] = useState(true);

  return (
    <Box
      className="voxel-dog"
      position="relative"
      m="auto"
      mt={["-20px", "-60px", "-120px"]}
      mb={["-40px", "-140px", "-200px"]}
      h={[280, 480, 640]}
      w={[280, 480, 640]}
    >
      {loading && (
        <Spinner
          size="xl"
          position="absolute"
          top="50%"
          left="50%"
          mt="calc(0px - var(--spinner-size))"
          ml="calc(0px - var(--spinner-size) / 2)"
        />
      )}
    </Box>
  );
};

export default Voxel;
