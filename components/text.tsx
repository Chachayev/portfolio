import React, { useEffect, useState } from "react";
import { useTypedSuperPower } from "../hooks/useTypedSuperPower";

const superpowers = [
  "Hello, I'm a front-end developer based in Turkmenistan!",
  "You're Welcome!",
];

const Text: React.FC = () => {
  const { typedSuperpower, selectedSuperpower } =
    useTypedSuperPower(superpowers);

  return <span className="blinking-cursor">{typedSuperpower}</span>;
};

export default Text;
