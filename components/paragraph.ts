import { Box } from "@chakra-ui/react";
import styled from "@emotion/styled";

const Paragraph = styled(Box)`
  text-align: justify;
  text-indent: 1em;
`;

export default Paragraph;
