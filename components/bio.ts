import { Box } from "@chakra-ui/react";
import styled from "@emotion/styled";

export const BioSection = styled(Box)`
  padding-left: 3.4em;
  text-indent: -3.4em;
  font-family: "'M PLUS Rouded 1c', sans-serif";
`;

export const BioYear = styled.span`
  font-weight: bold;
  margin-right: 1em;
  font-family: "'M PLUS Rouded 1c', sans-serif";
`;
