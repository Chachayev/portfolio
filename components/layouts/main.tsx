import React from "react";
import Head from "next/head";
import { Box, Container } from "@chakra-ui/react";
import { Router } from "next/router";
import Header from "../header";
import NoSsr from "../no-ssr";
import Voxel from "../voxel-dog";

type Props = {
  router: Router;
};

interface IProps {
  children: React.ReactNode;
  router: { asPath: string };
}

const Main: React.FC<IProps> = ({ children, router }) => {
  return (
    <Box as="main" pb={8}>
      <Head>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <title>Ahmet Chachayev - Homepage</title>
      </Head>
      <Header path={router.asPath} />
      <Container maxW="container.md" pt={14}>
        <NoSsr>
          <Voxel />
        </NoSsr>
        {children}
      </Container>
    </Box>
  );
};

export default Main;
