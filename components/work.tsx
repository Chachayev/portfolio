import React from "react";
import NextLink from "next/link";
import { Heading, Box, Image, Link, Badge } from "@chakra-ui/react";
import { BiChevronRight } from "react-icons/bi";

type Props = {
  children: React.ReactNode;
  href?: string;
  title?: string;
};

type WorkProps = {
  src: string;
  alt: string;
};

export const Title: React.FC<Props> = ({ children, href, title }) => (
  <Box display="flex">
    <NextLink href={{ pathname: href }}>
      <Link>{title}</Link>
    </NextLink>
    <span>
      <BiChevronRight />
    </span>
    <Heading as="h3" display="inline-block" fontSize={20} mb={4}>
      {children}
    </Heading>
  </Box>
);

export const WorkImage: React.FC<WorkProps> = ({ src, alt }) => (
  <Image src={src} alt={alt} borderRadius="lg" w="full" mb={4} />
);

export const Meta: React.FC<Props> = ({ children }) => (
  <Badge colorScheme="green" mr={2}>
    {children}
  </Badge>
);
