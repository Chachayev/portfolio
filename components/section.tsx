import React from "react";
import { motion } from "framer-motion";
import { chakra, shouldForwardProp } from "@chakra-ui/react";

type Props = {
  children: React.ReactNode;
  delay?: number;
};

const StyledDiv = chakra(motion.div, {
  shouldForwardProp(prop: string): boolean {
    return shouldForwardProp(prop) || prop === "transition";
  },
});

const Section: React.FC<Props> = ({ children }) => (
  <StyledDiv
    initial={{ y: 10, opacity: 0 }}
    animate={{ y: 0, opacity: 1 }}
    mb={6}
  >
    {children}
  </StyledDiv>
);

export default Section;
