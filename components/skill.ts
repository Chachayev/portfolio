import { Box } from "@chakra-ui/react";
import styled from "@emotion/styled";

export const SkillSection = styled(Box)`
  font-family: "'M PLUS Rouded 1c', sans-serif";
  padding-left: 1.5em;
`;

export const SkillName = styled.span`
  margin-right: 1em;
  font-weight: bold;
  font-family: "'M PLUS Rouded 1c', sans-serif";
`;
