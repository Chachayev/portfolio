import type { NextPage } from "next";
import Section from "../components/section";
import {
  Container,
  Box,
  Heading,
  SimpleGrid,
  Divider,
  Link,
} from "@chakra-ui/react";
import { SkillGridItem } from "../components/grid-item";
import Paragraph from "../components/paragraph";
import { SkillSection, SkillName } from "../components/skill";
import thumbFCC from "../public/images/certif/freecodecamp.png";
import thumbGL from "../public/images/certif/certif-1.jpg";
import thumbCOBH from "../public/images/certif/certif-2.jpeg";
import thumbCOBW from "../public/images/certif/certif-3.jpeg";

const Skills: NextPage = () => {
  return (
    <Container>
      <Heading
        as="h3"
        fontFamily="'M PLUS Rounded 1c', sans-serif"
        mb={6}
        fontSize={20}
        variant="section-title"
      >
        Skills
      </Heading>
      <Section>
        <SkillSection fontFamily="'M PLUS Rounded 1c', sans-serif">
          <SkillName>
            <Link href="https://typescript.com" target="_blank">
              Typescript
            </Link>
            :
          </SkillName>
          It&apos;s modern javascript language for front-end and back-end
          developers.
        </SkillSection>
        <SkillSection fontFamily="'M PLUS Rounded 1c', sans-serif">
          <SkillName>
            <Link href="https://nextjs.com" target="_blank">
              NextJS
            </Link>
            :
          </SkillName>
          Modified framework from react library.
        </SkillSection>
      </Section>
      <Section>
        <SkillSection fontFamily="'M PLUS Rounded 1c', sans-serif">
          <SkillName>
            <Link href="https://tailwindcss.com" target="_blank">
              TailwindCSS
            </Link>
            :
          </SkillName>
          Inline stylying for any nodeJS framework&apos;s.
        </SkillSection>
        <SkillSection>
          <SkillName>
            <Link href="https://chakraui.com" target="_blank">
              ChakraUI
            </Link>
            :
          </SkillName>
          Simple, beautiful and light weight css framework.
        </SkillSection>
      </Section>
      <Section>
        <SkillSection fontFamily="'M PLUS Rounded 1c', sans-serif">
          <SkillName>
            <Link href="https://reduxjs.com" target="_blank">
              Redux && RTKQ
            </Link>
            :
          </SkillName>
          State management for nodeJS framework&apos;s.
        </SkillSection>
        <SkillSection fontFamily="'M PLUS Rounded 1c', sans-serif">
          <SkillName>
            <Link href="https://postgresql.com" target="_blank">
              PostgreSQL
            </Link>
            :
          </SkillName>
          Power, simple and free database.
        </SkillSection>
      </Section>
      <Section>
        <Heading
          as="h3"
          fontSize={20}
          variant="section-title"
          mb={6}
          fontFamily="'M PLUS Rouneded 1c', sans-serif"
        >
          Certificates
        </Heading>
        <SimpleGrid columns={[1, 1, 2]} gap={6}>
          <Section>
            <SkillGridItem
              title="FreeCodeCamp"
              thumbnail={thumbFCC}
              href="https://freecodecamp.org/certification/Chachayev/front-end-development-libraries"
            >
              Front-end developer certificate.
            </SkillGridItem>
            <SkillGridItem
              href="https://eduonix.com/certificate/4a68e41794"
              title="Certificate Of Brinlliance"
              thumbnail={thumbCOBH}
            >
              White hat hacker, pentester.
            </SkillGridItem>
          </Section>
          <Section>
            <SkillGridItem
              title="Great Learning"
              href="https://verify.mygreatlearning.com/RUOQRXZR"
              thumbnail={thumbGL}
            >
              Reactjs developer.
            </SkillGridItem>
            <SkillGridItem
              title="Certificate Of Brinlliance"
              href="https://eduonix.com/certificate/d3dda2c24e"
              thumbnail={thumbCOBW}
            >
              Web developer.
            </SkillGridItem>
          </Section>
        </SimpleGrid>
      </Section>
    </Container>
  );
};

export default Skills;
