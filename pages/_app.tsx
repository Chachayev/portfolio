import type { AppProps } from "next/app";
import { GlobalStyles } from "../style/global-styles";
import { ChakraProvider } from "@chakra-ui/react";
import Layout from "../components/layouts/main";
import theme from "../libs/theme";
import Fonts from "../components/fonts";
import { AnimatePresence } from "framer-motion";

const Website = ({ Component, pageProps, router }: AppProps) => {
  return (
    <ChakraProvider theme={theme}>
      <Fonts />
      <Layout router={router}>
        <AnimatePresence exitBeforeEnter initial={true}>
          <Component {...pageProps} key={router.route} />
        </AnimatePresence>
      </Layout>
      <GlobalStyles />
    </ChakraProvider>
  );
};

export default Website;
