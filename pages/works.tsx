import type { NextPage } from "next";
import { Container, Heading, SimpleGrid, Divider } from "@chakra-ui/react";
import Section from "../components/section";
import { WorkGridItem } from "../components/grid-item";
import thumbChesstron from "../public/images/works/chess.jpg";
import thumbGaragum from "../public/images/works/garagum.jpg";
import thumbSoon from "../public/images/works/soon.png";
import thumbSnake from "../public/images/works/snake-game.png";
import thumbReactChat from "../public/images/works/react-chat.png";

const Works: NextPage = () => {
  return (
    <Container>
      <Heading
        as="h3"
        variant="page-title"
        fontSize={20}
        mb={4}
        fontFamily="'M PLUS Rounded 1c', sans-serif"
      >
        Work
      </Heading>
      <SimpleGrid columns={[1, 1, 2]} gap={6}>
        <Section>
          <WorkGridItem
            id="chesstron"
            href="https://google.com"
            title="Chesstron"
            thumbnail={thumbChesstron}
          >
            Simple chess game for PC for linux OS.
          </WorkGridItem>
        </Section>
        <Section>
          <WorkGridItem id="garagum" title="Garagum" thumbnail={thumbGaragum}>
            Developer&apos;s startup webpage.
          </WorkGridItem>
        </Section>
        <Section>
          <WorkGridItem
            id="snake-game"
            title="Snake Game"
            thumbnail={thumbSnake}
          >
            Snake game written with rust language.
          </WorkGridItem>
        </Section>
        <Section>
          <WorkGridItem
            id="react-chat"
            title="Simple chat"
            thumbnail={thumbReactChat}
          >
            Simple chat website.
          </WorkGridItem>
        </Section>
        <Section>
          <WorkGridItem id="#" title="Marketplace" thumbnail={thumbSoon}>
            Fully worked marketplace with online payment&apos;s rest api.
          </WorkGridItem>
        </Section>
      </SimpleGrid>
    </Container>
  );
};

export default Works;
