import type { NextPage } from "next";
import {
  Container,
  Box,
  Heading,
  Image,
  useColorModeValue,
  Link,
  Button,
  List,
  ListItem,
  Icon,
} from "@chakra-ui/react";
import Section from "../components/section";
import Paragraph from "../components/paragraph";
import NextLink from "next/link";
import { FaChevronRight } from "react-icons/fa";
import { BioSection, BioYear } from "../components/bio";
import Layout from "../components/layouts/article";
import {
  IoLogoTwitter,
  IoLogoDiscord,
  IoLogoGitlab,
  IoLogoSlack,
  IoLogoGithub,
} from "react-icons/io5";
import Text from "../components/text";

const Home: NextPage = () => {
  return (
    <Layout>
      <Container fontFamily="'M PLUS Rounded 1c', sans-serif">
        <Box
          borderRadius="lg"
          bg={useColorModeValue("whiteAlpha.500", "whiteAlpha.200")}
          mb={6}
          p={3}
          textAlign="center"
        >
          <Text />
        </Box>
        <Box display={{ md: "flex" }}>
          <Box flexGrow={1}>
            <Heading
              as="h2"
              variant="page-title"
              fontFamily="'M PLUS Rounded 1c', sans-serif"
            >
              Ahmet Chachayev
            </Heading>
            <p>Craftzman ( Developer / Designer )</p>
          </Box>
          <Box
            flexShrink={0}
            mt={{ base: 4, md: 0 }}
            ml={{ md: 6 }}
            textAlign="center"
          >
            <Image
              src="/images/ahmet.jpg"
              borderColor="whiteAlpha.800"
              borderStyle="solid"
              borderWidth={2}
              maxWidth="100px"
              display="inline-block"
              borderRadius="full"
              alt="Profile image"
            />
          </Box>
        </Box>
        <Section delay={0.1}>
          <Heading
            as="h3"
            variant="section-title"
            fontFamily="'M PLUS Rounded 1c', sans-serif"
          >
            Work
          </Heading>
          <Paragraph fontFamily="'M PLUS Rounded 1c', sans-serif">
            Ahmet is a front-end developer based in Ashgabat with a passion for
            building services/tuff he wants. In his free time, he studies Rust
            language, studies back-end technologies. He is currently developing
            developers startup homepage product called{" "}
            <NextLink href={{ pathname: "/works/garagum" }}>
              <Link>Garagum</Link>
            </NextLink>
            .
          </Paragraph>
          <Paragraph>
            <Box textAlign="center" my={4}>
              <NextLink href={{ pathname: "/works" }}>
                <Button rightIcon={<FaChevronRight />} colorScheme="teal">
                  My portfolio
                </Button>
              </NextLink>
            </Box>
          </Paragraph>
        </Section>
        <Section>
          <Heading
            as="h3"
            variant="section-title"
            fontFamily="'M PLUS Rounded 1c', sans-serif"
          >
            Bio
          </Heading>
          <BioSection fontFamily="'M PLUS Rounded 1c', sans-serif">
            <BioYear>2005</BioYear>
            Born in Ashgabat (Aşgabat), Turkmenistan.
          </BioSection>
          <BioSection fontFamily="'M PLUS Rounded 1c', sans-serif">
            <BioYear>2018</BioYear>
            Begin to learing Html, Css and Javascript and build my first landing
            page website with Html and Css.
          </BioSection>
          <BioSection fontFamily="'M PLUS Rounded 1c', sans-serif">
            <BioYear>2022</BioYear>
            Complete FreeCodeCamp Front-end course with Reactjs, Scss,
            Bootstrap, Redux and etc.
          </BioSection>
          <BioSection fontFamily="'M PLUS Rounded 1c', sans-serif">
            <BioYear>2022</BioYear>
            Start learning about CS from OSS University.
          </BioSection>
        </Section>
        <Section>
          <Heading
            as="h3"
            variant="section-title"
            fontFamily="'M PLUS Rounded 1c', sans-serif"
          >
            I love
          </Heading>
          <Paragraph fontFamily="'M PLUS Rounded 1c', sans-serif">
            Low-level programming, <Link href="#">Blockchain</Link>, OS
            development, <Link href="#">Pentesting</Link> .
          </Paragraph>
        </Section>
        <Section>
          <Heading
            as="h3"
            variant="section-title"
            fontFamily="'M PLUS Rounded 1c', sans-serif"
          >
            On the web
          </Heading>
          <List>
            <ListItem>
              <Link href="https://gitlab.com/Chachayev" target="_blank">
                <Button
                  variant="ghost"
                  colorScheme="teal"
                  leftIcon={<Icon as={IoLogoGitlab} />}
                >
                  @Chachayev
                </Button>
              </Link>
              <Link href="https://github.com/Chachayev" target="_blank">
                <Button
                  variant="ghost"
                  colorScheme="teal"
                  leftIcon={<Icon as={IoLogoGithub} />}
                >
                  @Chachayev
                </Button>
              </Link>
            </ListItem>
          </List>
        </Section>
      </Container>
    </Layout>
  );
};

export default Home;
