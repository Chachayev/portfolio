import { Badge, Container, Link, List, ListItem } from "@chakra-ui/react";
import type { NextPage } from "next";
import { HiExternalLink } from "react-icons/hi";
import Layout from "../../components/layouts/article";
import Paragraph from "../../components/paragraph";
import { Meta, Title, WorkImage } from "../../components/work";

const ReactChact: NextPage = () => {
  return (
    <Layout title="Simple react chat">
      <Container>
        <Title title="Works" href="/works">
          Chat <Badge>2022</Badge>
        </Title>
        <Paragraph>Simple reactJS web chat app.</Paragraph>
        <List mt={4} my={4}>
          <ListItem display="flex" alignItems="center">
            <Meta>Website</Meta>
            <Link
              href="https://github.com/AhmetChachayev/simple-chat"
              display="flex"
              alignItems="center"
              target="_blank"
            >
              View source <HiExternalLink />
            </Link>
          </ListItem>
          <ListItem>
            <Meta>Platform</Meta>
            <span>Browser ( Website )</span>
          </ListItem>
          <ListItem>
            <Meta>Stack</Meta>
            <span>React, Javascript, ExpressJS, Socket.io</span>
          </ListItem>
        </List>
        <WorkImage src="/images/works/react-chat.png" alt="React chat" />
      </Container>
    </Layout>
  );
};

export default ReactChact;
