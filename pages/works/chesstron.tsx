import type { NextPage } from "next";
import { Box, Container, List, ListItem, Link, Badge } from "@chakra-ui/react";
import { Title, Meta, WorkImage } from "../../components/work";
import Layout from "../../components/layouts/article";
import { HiExternalLink } from "react-icons/hi";
import Paragraph from "../../components/paragraph";

const Chesstron: NextPage = () => {
  return (
    <Layout title="Chesstron">
      <Container>
        <Title title="Works" href="/works">
          Chesstron <Badge>2022</Badge>
        </Title>
        <Paragraph>Simple gui chess game for linux OS.</Paragraph>
        <Paragraph>Usage:</Paragraph>
        <Paragraph>
          Step 1. git clone https://gitlab.com/Chachayev/Chess-game.git
        </Paragraph>
        <Paragraph>
          Step 2. cd ./Chess-game && chmod a+x dist/Chesstron.AppImage
        </Paragraph>
        <Paragraph>Step 3. ./Chesstron.AppImage</Paragraph>
        <List mt={4} my={4}>
          <ListItem display="flex" alignItems="center">
            <Meta>Website</Meta>
            <Link
              href="https://gitlab.com/Chachayev/Chess-game"
              display="flex"
              alignItems="center"
              target="_blank"
            >
              https://gitlab.com/Chachayev/Chess-game <HiExternalLink />
            </Link>
          </ListItem>
          <ListItem>
            <Meta>Platform</Meta>
            <span>Linux ( arch, x86-64 )</span>
          </ListItem>
          <ListItem>
            <Meta>Stack</Meta>
            <span>Typescript, Next, Electron</span>
          </ListItem>
        </List>
        <WorkImage src="/images/works/chess.jpg" alt="Chesstron" />
      </Container>
    </Layout>
  );
};

export default Chesstron;
