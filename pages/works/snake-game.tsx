import type { NextPage } from "next";
import { Container, Badge, List, ListItem, Link } from "@chakra-ui/react";
import Layout from "../../components/layouts/article";
import { Meta, Title, WorkImage } from "../../components/work";
import Paragraph from "../../components/paragraph";
import { HiExternalLink } from "react-icons/hi";

const SnakeGame: NextPage = () => {
  return (
    <Layout title="Snake game">
      <Container>
        <Title title="Works" href="/works">
          Snake game <Badge>2023</Badge>
        </Title>
        <Paragraph>Simple snake game.</Paragraph>
        <List mt={4} my={4}>
          <ListItem display="flex" alignItems="center">
            <Meta>Game</Meta>
            <Link
              href="https://github.com/AhmetChachayev/snake-game"
              display="flex"
              alignItems="center"
              target="_blank"
            >
              View source <HiExternalLink />
            </Link>
          </ListItem>
          <ListItem>
            <Meta>Platform</Meta>
            <span>PC from Windows, macOS and Linux ( x86-64 ).</span>
          </ListItem>
          <ListItem>
            <Meta>Stack</Meta>
            <span>Rust language</span>
          </ListItem>
        </List>
        <WorkImage src="/images/works/snake-game.png" alt="Snake game" />
      </Container>
    </Layout>
  );
};

export default SnakeGame;
