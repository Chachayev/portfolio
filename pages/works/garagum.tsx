import type { NextPage } from "next";
import { Container, Badge, Link, List, ListItem } from "@chakra-ui/react";
import { Title, Meta, WorkImage } from "../../components/work";
import Layout from "../../components/layouts/article";
import Paragraph from "../../components/paragraph";
import { HiExternalLink } from "react-icons/hi";

const Work: NextPage = () => {
  return (
    <Layout title="Garagum">
      <Container>
        <Title title="Works" href="/works">
          Garagum <Badge>2022</Badge>
        </Title>
        <Paragraph>A startup programmers company website.</Paragraph>
        <List mt={4} my={4}>
          <ListItem display="flex" alignItems="center">
            <Meta>Website</Meta>
            <Link
              href="https://garagum.vercel.app"
              display="flex"
              alignItems="center"
              target="_blank"
            >
              https://garagum.vercel.app/ <HiExternalLink />
            </Link>
          </ListItem>
          <ListItem>
            <Meta>Platform</Meta>
            <span>Browser ( Website )</span>
          </ListItem>
          <ListItem>
            <Meta>Stack</Meta>
            <span>Typescript, Next, Tailwindcss</span>
          </ListItem>
        </List>
        <WorkImage src="/images/works/garagum.jpg" alt="Garagum" />
        <WorkImage src="/images/works/garagum.jpg" alt="Garagum" />
      </Container>
    </Layout>
  );
};

export default Work;
